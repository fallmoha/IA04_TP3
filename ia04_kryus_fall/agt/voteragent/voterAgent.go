package agt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ia04_kryus_fall/agt"
	agt2 "ia04_kryus_fall/agt/ballotagent"
	"ia04_kryus_fall/comsoc"
	"net/http"
)

type VoterID int

type RestVoterAgent struct {
	id       VoterID
	ballotID agt2.BallotID
	rule     string
	prefs    []comsoc.Alternative
	options  []int
	url      string
}

type RestVoterI interface {
}

func NewRestClientAgent(id int, prefs []comsoc.Alternative, rule string, ballotID int, options []int, url string) *RestVoterAgent {
	return &RestVoterAgent{VoterID(id), agt2.BallotID(ballotID), rule, prefs, options, url}
}

func (rva *RestVoterAgent) treatResponseResult(r *http.Response) agt.ResponseResult {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp agt.ResponseResult
	json.Unmarshal(buf.Bytes(), &resp)

	return resp
}

func (rva *RestVoterAgent) treatResponseNewBallot(r *http.Response) agt.ResponseNewBallot {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp agt.ResponseNewBallot
	json.Unmarshal(buf.Bytes(), &resp)

	return resp
}

func (rva *RestVoterAgent) DoRequestVote() (err error) {
	req := agt.RequestVote{
		AgentID: fmt.Sprintf("ag_id%d", rva.id),
		VoteID:  fmt.Sprintf("vote%d", rva.ballotID),
		Prefs:   rva.prefs,
		Options: rva.options,
	}

	// sérialisation de la requête
	url := rva.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	return
}

func (rva *RestVoterAgent) DoRequestResult() (res agt.ResponseResult, err error) {
	req := agt.RequestResult{
		BallotID: fmt.Sprintf("vote%d", rva.ballotID),
	}

	// sérialisation de la requête
	url := rva.url + "/result"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rva.treatResponseResult(resp)

	return
}

func (rva *RestVoterAgent) DoRequestNewBallot(deadline string, voterIDs []string, alts int) (res agt.ResponseNewBallot, err error) {
	req := agt.RequestNewBallot{
		Rule:     rva.rule,
		Deadline: deadline,
		VoterIDs: voterIDs,
		Alts:     alts,
	}

	// sérialisation de la requête
	url := rva.url + "/new_ballot"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusCreated {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rva.treatResponseNewBallot(resp)

	return
}
