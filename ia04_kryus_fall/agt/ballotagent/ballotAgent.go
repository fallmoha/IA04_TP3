package agt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ia04_kryus_fall/agt"
	"ia04_kryus_fall/comsoc"
	"log"
	"net/http"
	"sync"
	"time"
)

type BallotID int

type RestBallotAgent struct {
	sync.Mutex
	ID BallotID

	addr string // the only non-varying value in one
	// server instance, the rest is change
	// every RequestNewBallot
	pfl      comsoc.Profile
	rule     string
	isOpen   bool // open: 1 / closed: 0
	voterIDs []string
	alts     []comsoc.Alternative
	options  []int // only thresholds for now
	count    comsoc.Count
	bestAlt  comsoc.Alternative

	votedMap map[string]int // voted: 1 / hasn't voted: 0
}

type RestBallotI interface {
	checkMethod(method string, w http.ResponseWriter, r *http.Request) bool
	decodeRequestNewBallot(r *http.Request) (req agt.RequestNewBallot, err error)
	decodeRequestVote(r *http.Request) (req agt.RequestVote, err error)
	decodeRequestResult(r *http.Request) (req agt.RequestResult, err error)
}

func NewRestBallotAgent(addr string) *RestBallotAgent {
	return &RestBallotAgent{ID: BallotID(0), addr: addr, isOpen: true}
}

// Test de la méthode
func (rsa *RestBallotAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "405: HTTP method '%q' not allowed for this call.", r.Method)
		return false
	}
	return true
}

func (*RestBallotAgent) decodeRequestNewBallot(r *http.Request) (req agt.RequestNewBallot, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestBallotAgent) decodeRequestVote(r *http.Request) (req agt.RequestVote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestBallotAgent) decodeRequestResult(r *http.Request) (req agt.RequestResult, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rba *RestBallotAgent) doNewBallot(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rba.Lock()
	defer rba.Unlock()

	log.Println("NEW BALLOT: Received request of new ballot...")

	// vérification de la méthode de la requête
	if !rba.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rba.decodeRequestNewBallot(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		log.Println("NEW BALLOT: Error encountered during decoding of the new ballot request.")
		return
	}
	log.Println("NEW BALLOT: Request for a new ballot decoded.")

	// traitement de la requête
	var resp agt.ResponseNewBallot

	rba.ID += 1
	log.Println("NEW BALLOT: New ballot id: ", rba.ID)

	rba.rule = req.Rule
	rba.voterIDs = req.VoterIDs
	rba.votedMap = make(map[string]int)
	rba.alts = make([]comsoc.Alternative, req.Alts)
	for i := 0; i < req.Alts; i++ {
		rba.alts[i] = comsoc.Alternative(i + 1)
	}
	log.Println("NEW BALLOT: Number of alternatives: ", req.Alts)

	if rba.rule != "majority" && rba.rule != "borda" && rba.rule != "copeland" && rba.rule != "condorcet" && rba.rule != "approval"  && rba.rule != "single transferable vote" && rba.rule != "scoring" {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("501: vote rule \"'%s'\" is not impemented in the system", req.Rule)
		w.Write([]byte(msg))
		return
	}
	log.Println("NEW BALLOT: Ballot rule set to ", req.Rule)

	deadline, err := time.Parse("Mon Jan _2 15:04:05 MST 2006", req.Deadline) // Unixdate layout
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("400: Deadline \"'%s'\" is not in a correct format", req.Deadline)
		w.Write([]byte(msg))
		return
	}
	log.Println("NEW BALLOT: Voting deadline for all voters: ", req.Deadline)

	// close vote after deadline
	_ = time.AfterFunc(time.Until(deadline), func() {
		rba.isOpen = false
		var bestAlts []comsoc.Alternative
		switch rba.rule {
		case "borda":
			rba.count, err = comsoc.BordaSWF(rba.pfl)
			bestAlts, err = comsoc.BordaSCF(rba.pfl)
		case "copeland":
			rba.count, err = comsoc.CopelandSWF(rba.pfl)
			bestAlts, err = comsoc.CopelandSCF(rba.pfl)
		case "majority":
			rba.count, err = comsoc.MajoritySWF(rba.pfl)
			bestAlts, err = comsoc.MajoritySCF(rba.pfl)
		case "approval":
			rba.count, err = comsoc.ApprovalSWF(rba.pfl, rba.options)
			bestAlts, err = comsoc.ApprovalSCF(rba.pfl, rba.options)
		case "single transferable vote":
			rba.count, err = comsoc.SingleTransferableVoteSWF(rba.pfl)
			bestAlts, err = comsoc.SingleTransferableVoteSCF(rba.pfl)
		case "scoring":
			rba.count, err = comsoc.ScoringSWF(rba.pfl, rba.options)
			bestAlts, err = comsoc.ScoringSCF(rba.pfl, rba.options)
		}
		// the tie-break is in the order of rba.alts
		tb := comsoc.TieBreakFactory(rba.alts)
		rba.bestAlt, err = tb(bestAlts)

		if err != nil {
			log.Println("Internal server error in results calculation.", err)
			w.WriteHeader(http.StatusInternalServerError)
			msg := fmt.Sprintf("500: internal server error.")
			w.Write([]byte(msg))
			return
		}
		fmt.Println("TIME'S UP! The", rba.rule, "vote is closed and the results are in.\nThe final count is :", rba.count)
	})

	resp.BallotID = fmt.Sprintf("vote%d", rba.ID)
	log.Printf("NEW BALLOT: Ballot '%s' created.", resp.BallotID)
	w.WriteHeader(http.StatusCreated) // 201
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rba *RestBallotAgent) doVote(w http.ResponseWriter, r *http.Request) {
	rba.Lock()
	defer rba.Unlock()

	log.Println("VOTE: Received a voting request...")

	// Request method checking
	if !rba.checkMethod("POST", w, r) {
		return
	}

	// Request decoding
	req, err := rba.decodeRequestVote(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		log.Println("VOTE: Error encountered during decoding of the voting request.")
		return
	}
	log.Println("VOTE: Request for a vote decoded.")

	// Check if the vote is done past the deadline
	if !rba.isOpen {
		w.WriteHeader(http.StatusServiceUnavailable)
		msg := fmt.Sprintf("503: The ballot '%d' is closed, votes past the set deadline cannot be counted", rba.ID)
		w.Write([]byte(msg))
		return
	}
	log.Println("VOTE: Voting is ongoing.")

	// ballot id checking
	if fmt.Sprintf("vote%d", rba.ID) != req.VoteID {
		log.Println("VOTE: the ballot ID is not an open one.")
		w.WriteHeader(http.StatusNotFound)
		msg := fmt.Sprintf("404: ballot id '%s' not found.", req.VoteID)
		w.Write([]byte(msg))
		return
	}
	log.Println("VOTE: BallotID: ", req.VoteID)

	// Check if the voter didn't already vote
	if rba.votedMap[req.AgentID] != 0 {
		w.WriteHeader(http.StatusForbidden)
		msg := fmt.Sprintf("403: Voter '%s' has already voted", req.AgentID)
		w.Write([]byte(msg))
		return
	}
	log.Printf("VOTE: Voter '%s' has not voted yet.", req.AgentID)

	// Process options case

	if (len(req.Options) > 0 && rba.rule != "approval" && rba.rule != "scoring") || (len(req.Options) > 1 && rba.rule == "approval") { // modify conditions according to options implemented
		log.Println("VOTE: Error in given options.")
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("501: Approval threshold only accept threshold for approval (in a list, example: 'options: [3]' in JSON).")
		w.Write([]byte(msg))
		return
	}
	if rba.rule == "approval" {
		rba.options = append(rba.options, int(req.Options[0]))
	} else if rba.rule == "scoring" {
		rba.options = req.Options
	}

	log.Println(rba.pfl)
	log.Println(req.Prefs)
	// Check if the list of preference is in accordance with the profile
	if rba.pfl != nil && comsoc.CheckProfileAlternative(rba.pfl, req.Prefs) != nil {
		log.Println("VOTE: Error in preferences.")
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("401: Preferences of voter '%s' are not correct in the candidates profile.", req.AgentID)
		w.Write([]byte(msg))
		return
	}
	log.Println("VOTE: List of preferences given is valid.")

	rba.pfl = append(rba.pfl, req.Prefs)
	rba.votedMap[req.AgentID] = 1

	w.WriteHeader(http.StatusOK)
	msg := fmt.Sprintf("200: OK. '%s''s vote has been taken in account.", req.AgentID)
	w.Write([]byte(msg))
	return
}

func (rba *RestBallotAgent) doResult(w http.ResponseWriter, r *http.Request) {
	rba.Lock()
	defer rba.Unlock()

	// vérification de la méthode de la requête
	if !rba.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rba.decodeRequestResult(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("Error encountered during decoding of the result request.")
		fmt.Fprint(w, err.Error())
		return
	}
	log.Println("RESULT: Request for the result of ballot ", req.BallotID, " decoded.")

	// ballot id checking
	if fmt.Sprintf("vote%d", rba.ID) != req.BallotID {
		w.WriteHeader(http.StatusNotFound)
		msg := fmt.Sprintf("404: ballot id '%s' not found.", req.BallotID)
		w.Write([]byte(msg))
		return
	}
	log.Println("RESULT: Ballot ID: ", req.BallotID)

	// Check if it is too early to publish the results (deadline still not met)
	if rba.isOpen {
		w.WriteHeader(http.StatusTooEarly)
		msg := fmt.Sprintf("425: The ballot '%s' is still open, result will be available past the set deadline", req.BallotID)
		w.Write([]byte(msg))
		return
	}
	log.Println("RESULT: Vote given in time.")

	// traitement de la requête
	var resp agt.ResponseResult

	resp.Winner = int(rba.bestAlt) // gérer les tie-break
	resp.Ranking = comsoc.Ranking(rba.count)

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rba *RestBallotAgent) Start() {
	// création du routeur
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rba.doNewBallot)
	mux.HandleFunc("/vote", rba.doVote)
	mux.HandleFunc("/result", rba.doResult)

	// création du serveur http
	s := &http.Server{
		Addr:           rba.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rba.addr)
	go log.Fatal(s.ListenAndServe())
}
