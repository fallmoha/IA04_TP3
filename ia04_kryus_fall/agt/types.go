package agt

import (
	"ia04_kryus_fall/comsoc"
)

type RequestVote struct {
	AgentID string               `json:"agent-id"`
	VoteID  string               `json:"vote-id"`
	Prefs   []comsoc.Alternative `json:"prefs"`
	Options []int                `json:"options"`
}

type RequestNewBallot struct {
	Rule     string   `json:"rule"`
	Deadline string   `json:"deadline"`
	VoterIDs []string `json:"voter-ids"`
	Alts     int      `json:"#alts"`
}

type RequestResult struct {
	BallotID string `json:"ballot-id"`
}

type ResponseNewBallot struct {
	BallotID string `json:"ballot-id"`
}

type ResponseResult struct {
	Winner  int                  `json:"winner"`
	Ranking []comsoc.Alternative `json:"ranking"`
}
