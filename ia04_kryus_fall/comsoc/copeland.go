package comsoc

func CopelandSWF(p Profile) (count Count, err error) {
	count = make(map[Alternative]int)
	firstProfile := p[0]
	// Generate every combination of two different alternatives.
	for _, couple := range generateCouples(firstProfile) {
		duel := Count(make(map[Alternative]int)) // un count
		alt1 := couple[0]
		alt2 := couple[1]

		// For two alternatives in a couple, we determine the overall winner for all profiles.
		// If it's a tiebreak (maxCount has a length of 2), no alternative gets a point.
		// Otherwise, the winner receives one and the other loses one.
		for _, voterAlts := range p {
			if isPref(alt1, alt2, voterAlts) {
				duel[alt1] += 1
			} else {
				duel[alt2] += 1
			}
		}
		profileWinner := maxCount(duel)

		if len(profileWinner) == 1 {
			if alt1 == profileWinner[0] {
				count[alt1] += 1
				count[alt2] -= 1
			} else if alt2 == profileWinner[0] {
				count[alt2] += 1
				count[alt1] -= 1
			}
		}
	}
	return
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, erreur := CopelandSWF(p)
	return maxCount(count), erreur
}
