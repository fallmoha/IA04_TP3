package comsoc

func ScoringSWF(p Profile, scores []int) (count Count, err error) {
	count = make(map[Alternative]int)
	for _, profile := range p {
		for i, alt := range profile {
			count[alt] += scores[len(profile)-1-i]
		}
	}
	return count, nil
}

func ScoringSCF(p Profile, scores []int) (bestAlts []Alternative, err error) {
	count, erreur := ScoringSWF(p, scores)
	return maxCount(count), erreur
}
