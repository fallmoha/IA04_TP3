package comsoc

func SingleTransferableVoteSWF(p Profile) (count Count, err error) {
	count = make(map[Alternative]int)
	numRound := 0
	for len(p[0]) > 0 {
		countRound := make(map[Alternative]int)
		for _, alts := range p {
			countRound[alts[0]] += 1
		}
		loserAlt := minCount(countRound)[0]
		count[loserAlt] = numRound
		p = retrieveAlt(p, loserAlt)
		numRound += 1
	}
	return count, nil
}

func SingleTransferableVoteSCF(p Profile) (bestAlts []Alternative, err error) {
	count, erreur := SingleTransferableVoteSWF(p)
	return maxCount(count), erreur
}
