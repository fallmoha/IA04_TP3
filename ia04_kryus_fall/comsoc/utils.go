package comsoc

import (
	"errors"
	"math"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if alt == v {
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return rank(alt1, prefs) < rank(alt2, prefs)
}

// renvoie les meilleures alternatives pour un décompte donné
func maxCount(count Count) (bestAlts []Alternative) {
	max := math.MinInt
	for k, v := range count {
		switch {
		case v == max:
			bestAlts = append(bestAlts, k)
		case v > max:
			bestAlts = []Alternative{k}
			max = v
		}
	}
	return
}

func Ranking(count Count) (ranking []Alternative) {
	for len(count) > 0 {
		best := maxCount(count)[0] // comment gérer le tie break
		ranking = append(ranking)
		delete(count, best)
	}
	return
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
// func checkProfile(prefs Profile) error

// vérifier que tout le monde a les mêmes alternatives, qu'il n'est pas vide

// CheckProfile vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func CheckProfile(prefs Profile) error {
	norm := len(prefs[0]) // Si un des profiles n'a pas la même taille: erreur
	for _, voter := range prefs {
		if len(voter) != norm {
			return errors.New("001: ProfileErrors")
		}
		check := make(map[Alternative]int)
		for _, alt := range voter {
			check[alt] += 1
			if check[alt] > 1 {
				return errors.New("001: ProfileErrors")
			}
		}
	}
	return nil
}

func CheckProfileAlternative(prefs Profile, alts []Alternative) error {
	present := func(alt Alternative, pref []Alternative) bool {
		for _, alt2 := range pref {
			if alt == alt2 {
				return true
			}
		}
		return false
	}
	err := CheckProfile(prefs)
	if err != nil {
		return err
	}

	for _, alt := range alts {
		for _, pref := range prefs {
			if !present(alt, pref) {
				return errors.New("001: ProfileErrors")
			}
		}
	}
	return nil
}

func generateCouples(alts []Alternative) (couples [][]Alternative) {
	if len(alts) == 2 {
		return [][]Alternative{alts}
	}
	first := alts[0]
	for _, second := range alts[1:] {
		couples = append(couples, []Alternative{first, second})
	}
	return append(couples, generateCouples(alts[1:])...)
}

func retrieveAlt(p Profile, a Alternative) Profile {
	for _, profile := range p {
		for i, alt := range profile {
			if alt == a {
				profile = append(profile[:i], profile[i+1:]...)
				break
			}
		}
	}
	return p
}

func minCount(count Count) (bestAlts []Alternative) {
	min := math.MaxInt
	for k, v := range count {
		switch {
		case v == min:
			bestAlts = append(bestAlts, k)
		case v < min:
			bestAlts = []Alternative{k}
			min = v
		}
	}
	return
}
