package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	count := Count(make(map[Alternative]int))
	firstProfile := p[0]
	// Generate every combination of two different alternatives.
	for _, couple := range generateCouples(firstProfile) {
		duel := Count(make(map[Alternative]int)) // un count
		alt1 := couple[0]
		alt2 := couple[1]

		// For two alternatives in a couple, we determine the overall winner for all profiles.
		// Nobody wins a tiebreak, which disqualifies both alternatives from ever being the Condorcet winner.
		for _, voterAlts := range p {
			if isPref(alt1, alt2, voterAlts) {
				duel[alt1] += 1
			} else {
				duel[alt2] += 1
			}
		}
		profileWinner := maxCount(duel)

		if len(profileWinner) == 1 {
			count[profileWinner[0]] += 1

			// Condorcet winner is the alternative that wins n-1 times.
			if count[profileWinner[0]] == len(firstProfile)-1 {
				return []Alternative{profileWinner[0]}, nil
			}
		}
	}
	return
}
