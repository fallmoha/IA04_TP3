package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	count = make(map[Alternative]int)
	for _, profile := range p {
		for i, alt := range profile {
			count[alt] += len(profile) - 1 - i
		}
	}
	return count, nil
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, erreur := BordaSWF(p)
	return maxCount(count), erreur
}
