package comsoc

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	count = make(map[Alternative]int)
	for i, profile := range p {
		for j, alt := range profile {
			if j+1 > thresholds[i] {
				break
			}
			count[alt] += 1
		}
	}
	return count, nil
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, erreur := ApprovalSWF(p, thresholds)
	return maxCount(count), erreur
}
