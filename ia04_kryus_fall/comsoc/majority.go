package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	count = make(map[Alternative]int)
	for _, alts := range p {
		count[alts[0]] += 1
	} // 0-votes alternatives are not included
	return count, nil
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, erreur := MajoritySWF(p)
	return maxCount(count), erreur
}
