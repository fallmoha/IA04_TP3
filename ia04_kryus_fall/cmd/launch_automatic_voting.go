package main

import (
	"fmt"
	agt "ia04_kryus_fall/agt/ballotagent"
	agt2 "ia04_kryus_fall/agt/voteragent"
	"ia04_kryus_fall/comsoc"
	"log"
	"math/rand"
	"time"
)

func shuffle(n int, pref []comsoc.Alternative) []comsoc.Alternative {
	rand.Seed(time.Now().UnixNano())
	// source := rand.NewSource(time.Now().UnixNano())
	// r := rand.New(source
	rand.Shuffle(n, func(i, j int) {
		pref[i], pref[j] = pref[j], pref[i]
	})
	return pref
}

func makeVector(n int) []int {
	vec := make([]int, n)
	for i, _ := range vec {
		vec[i] = 20 + i
	}
	return vec
}

func main() {
	const url1 = ":8080"
	const url2 = "http://localhost:8080"

	var choice int
	var rule string
	var ts int
	choice = -1
	ts = -1

	for choice < 0 || choice > 6 {

		fmt.Println("Choose the voting rule amonst")
		fmt.Println("0: Borda")
		fmt.Println("1: Majority")
		fmt.Println("2: Copeland")
		fmt.Println("3: Approval")
		fmt.Println("4: Single Transferable Vote")
		fmt.Println("5: Scoring (default vector is <20,21,22,23,...>)")
		fmt.Println("6: Randomly choose between all the latters")
		fmt.Println("Enter a number: ")
		fmt.Scanln(&choice)

		switch choice {
		case 0:
			rule = "borda"
		case 1:
			rule = "majority"
		case 2:
			rule = "copeland"
		case 3:
			rule = "approval"
			fmt.Println("Enter an integer number for the threshold : ")
			fmt.Scanln(&ts)
		case 4:
			rule = "single transferable vote"
		case 5:
			rule = "scoring"
		case 6:
			rules := []string{"borda", "approval", "copeland", "majority"}
			rand.Seed(time.Now().UnixNano())
			rule = rules[rand.Intn(4)]
			log.Println("MAIN:", rule, "has been taken randomly as the voting rule.")
		default:
			fmt.Println("Incorrect choice...")
		}
	}

	n := 4 // number of alternatives
	m := 4 // number of voters

	prefs := make([][]comsoc.Alternative, m)

	for i := 0; i < m; i++ {
		prefs[i] = make([]comsoc.Alternative, n)
		for j := 0; j < n; j++ {
			prefs[i][j] = comsoc.Alternative(j) + 1
		}
	}
	for i, pref := range prefs {
		prefs[i] = shuffle(n, pref)
	}

	voterIds := func(_length int) []string {
		voterIDs := make([]string, _length)
		for i, _ := range voterIDs {
			voterIDs[i] = fmt.Sprint("ag_id", i+1)
		}
		return voterIDs
	}(n)
	options := make([]int, 0)
	if ts != -1 {
		options = append(options, ts)
	}

	if rule == "scoring" {
		options = makeVector(n)
	}

	deadline := time.Now().Add(time.Second * 10)

	server := agt.NewRestBallotAgent(":8080")

	go server.Start()

	voterAgents := make([]agt2.RestVoterAgent, 0, m)
	for i := 0; i < m; i++ {
		id := i
		ballotID := 1
		regle := rule
		pref := prefs[i]
		opt := options
		vtr := agt2.NewRestClientAgent(id, pref, regle, ballotID, opt, url2)
		voterAgents = append(voterAgents, *vtr)
	}

	//log.Println("MAIN: Starting new ballot...")
	go func(voter agt2.RestVoterAgent) {
		ballot, err := voter.DoRequestNewBallot(deadline.Format("Mon Jan _2 15:04:05 MST 2006"), voterIds, n)
		if err != nil {
			log.Println("MAIN: Error while requesting new ballot. Error: ", err.Error())
		}
		log.Println("MAIN: New ballot created. JSON object: \n", ballot)
	}(voterAgents[0])

	time.Sleep(500 * time.Millisecond) // there is a peaceful time between the request of a new ballot and the moment when voters begin to rush

	//log.Println("MAIN: Starting voting process...")
	for i, vtr := range voterAgents {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		go func(vtr agt2.RestVoterAgent, index int) {
			err := vtr.DoRequestVote()
			if err != nil {
				log.Println("MAIN: Error while requesting vote for agent", index, ". Error: ", err.Error())
			}
		}(vtr, i)
	}

	_ = time.AfterFunc(time.Until(deadline), func() {
		//log.Println("MAIN: Requesting voting results...")
		res, err := voterAgents[0].DoRequestResult()
		if err != nil {
			log.Println("MAIN: Error while requesting voting results. Error: ", err.Error())
		}
		log.Println("Done. Results of the election:\n", res)
		return
	})

	//time.Sleep(time.Second * 20)
	fmt.Scanln()
}
