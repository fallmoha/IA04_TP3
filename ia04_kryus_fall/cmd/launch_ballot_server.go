package main

import (
	"fmt"
	agt "ia04_kryus_fall/agt/ballotagent"
)

func main() {
	server := agt.NewRestBallotAgent(":8080")
	server.Start()
	fmt.Scanln()
}
