# IA04 Projet TP3
#### KRYUS Antoine & FALL Mohamed



## Installation

```
go get gitlab.utc.fr/fallmoha/IA04_TP3
```

## Lancement du serveur

#### Lancer le serveur de scrutin de vote uniquement 
```
go run cmd\launch_ballot_server.go
```

#### Démo lançant un serveur de scrutin et un vote avec un nombre de voteurs determiné (retourne les résultats de l'élection, profil et méthode de scrutin au choix parmi celles disponibles) 
```
go run cmd\launch_automatic_voting.go
```

## Contenu du projet

#### Agents

- BallotAgent : Agent-serveur, reçoit les requêtes de nouveau scrutin, vote d'agent et de résultat de vote. 
- VoterAgent : Envoie les requêtes liées au scrutin.
#### Méthodes de vote

- Vote par approbation
- Méthode de Borda
- Gagnant de Condorcet
- Méthode de Copeland
- Scrutin majoritaire
- Single Transferable Vote
- Scoring
- \+ Fonction de tie-break 
